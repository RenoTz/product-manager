import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router } from '@angular/router';
import { of } from 'rxjs';
import { ClientService } from 'src/app/panier/services/client.service';
import { PanierService } from 'src/app/panier/services/panier.service';
import { Client } from 'src/app/shared/models/client.model';
import { Panier } from 'src/app/shared/models/panier.model';
import { User } from 'src/app/shared/models/user.model';
import { UserService } from './user.service';

@Injectable({
  providedIn: 'root'
})
export class UserResolverService implements Resolve<User> {

  constructor(
    public userService: UserService,
    private clientService: ClientService,
    private panierService: PanierService,
    private router: Router
  ) { }

  resolve(route: ActivatedRouteSnapshot): Promise<User> {

    const user = new User();

    return new Promise((resolve, reject) => {
      this.userService.getCurrentUser()
      .then(res => {
        if (res.providerData[0].providerId === 'password') {
          user.image = 'http://dsi-vd.github.io/patternlab-vd/images/fpo_avatar.png';
          user.name = res.displayName;
          user.provider = res.providerData[0].providerId;
          this.verifyAndCreatePanierIfNotExist(user.name);
          return resolve(user);
        } else {
          user.image = res.photoURL;
          user.name = res.displayName;
          user.provider = res.providerData[0].providerId;
          this.verifyAndCreatePanierIfNotExist(user.name);
          return resolve(user);
        }

      }, err => {
        this.router.navigate(['/login']);
        return reject(err);
      });
    });
  }

  private verifyAndCreatePanierIfNotExist(userName: string) {
    this.clientService.collection$.subscribe((result: Client[]) => {
      const currentClient = result.find((client: Client) => userName === client.name);
      if (currentClient && !currentClient.idPanier) {
        this.panierService.create().subscribe(data => {
          currentClient.idPanier = data;
          this.clientService.update(currentClient);
        });
      }
      if (currentClient && currentClient.idPanier) {
        this.panierService.getPanier(currentClient.idPanier).subscribe((data: Panier) => {
          this.panierService.panier$ = of(data);
        });
      }
    });
  }
}
