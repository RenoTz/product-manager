import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { UserComponent } from './containers/user/user.component';
import { PageUserComponent } from './pages/page-user/page-user.component';
import { UserRoutingModule } from './user-routing.module';

@NgModule({
  declarations: [PageUserComponent, UserComponent],
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule
  ]
})
export class UserModule { }
