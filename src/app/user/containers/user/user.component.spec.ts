import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AngularFirestore } from '@angular/fire/firestore';
import { ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { BehaviorSubject } from 'rxjs';
import { PanierService } from 'src/app/panier/services/panier.service';
import { UserService } from '../../services/user.service';
import { AuthService } from './../../../login/services/auth.service';
import { UserComponent } from './user.component';

describe('UserComponent', () => {
  const fakeRouter = jasmine.createSpyObj('Router', ['navigate']);
  const fakeAuthService = jasmine.createSpyObj('AuthService', ['doLogout']);
  const fakeUserService = jasmine.createSpyObj('UserService', ['update']);
  const fakePanierService = jasmine.createSpyObj('PanierService', ['create']);

  const FirestoreStub = {
    collection: (name: string) => ({
      doc: (_id: string) => ({
        valueChanges: () => new BehaviorSubject({ id: 'test', name: 'toto', email: 'mail@mail.com', idPanier: '666'}),
        set: (_d: any) => new Promise((resolve, _reject) => resolve()),
      }),
    }),
  };

  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserComponent],
      imports: [ReactiveFormsModule, RouterTestingModule],
      providers: [
        { provide: Router, useValue: fakeRouter },
        { provide: AuthService, useValue: fakeAuthService },
        { provide: UserService, useValue: fakeUserService },
        { provide: PanierService, useValue: fakePanierService },
        { provide: AngularFirestore, useValue: FirestoreStub }
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  xit('should create', () => {
    expect(component).toBeTruthy();
  });
});
