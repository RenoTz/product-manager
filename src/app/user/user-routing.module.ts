import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageUserComponent } from './pages/page-user/page-user.component';
import { UserResolverService } from './services/user-resolver.service';

const appRoutes: Routes = [
  {
    path: '',
    component: PageUserComponent,
    resolve: {
      data: UserResolverService
    }
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule]
})
export class UserRoutingModule {}
