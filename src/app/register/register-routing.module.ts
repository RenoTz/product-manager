import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PageRegisterComponent } from './pages/page-register/page-register.component';

const appRoutes: Routes = [
  { path: 'register', component: PageRegisterComponent }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(
      appRoutes
    )
  ]
})
export class RegisterRoutingModule { }
