import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { RegisterComponent } from './containers/register/register.component';
import { PageRegisterComponent } from './pages/page-register/page-register.component';

@NgModule({
  declarations: [RegisterComponent, PageRegisterComponent],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class RegisterModule { }
