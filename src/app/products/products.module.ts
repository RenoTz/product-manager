import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { FormProductComponent } from './components/form-product/form-product.component';
import { ItemProductComponent } from './components/item-product/item-product.component';
import { AddProductComponent } from './containers/add-product/add-product.component';
import { CommentProductComponent } from './containers/comment-product/comment-product.component';
import { DetailProductComponent } from './containers/detail-product/detail-product.component';
import { EditProductComponent } from './containers/edit-product/edit-product.component';
import { ListProductsComponent } from './containers/list-products/list-products.component';
import { PageAddProductComponent } from './pages/page-add-product/page-add-product.component';
import { PageEditProductComponent } from './pages/page-edit-product/page-edit-product.component';
import { PageProductsComponent } from './pages/page-products/page-products.component';
import { ProductsRoutingModule } from './products-routing.module';

@NgModule({
  declarations: [
    PageProductsComponent,
    ListProductsComponent,
    ItemProductComponent,
    DetailProductComponent,
    CommentProductComponent,
    PageAddProductComponent,
    AddProductComponent,
    FormProductComponent,
    PageEditProductComponent,
    EditProductComponent
  ],
  imports: [CommonModule, ProductsRoutingModule, SharedModule]
})
export class ProductsModule {}
