import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserResolverService } from '../user/services/user-resolver.service';
import { CommentProductComponent } from './containers/comment-product/comment-product.component';
import { DetailProductComponent } from './containers/detail-product/detail-product.component';
import { PageAddProductComponent } from './pages/page-add-product/page-add-product.component';
import { PageEditProductComponent } from './pages/page-edit-product/page-edit-product.component';
import { PageProductsComponent } from './pages/page-products/page-products.component';

const appRoutes: Routes = [
  {
    path: '',
    component: PageProductsComponent,
    resolve: {
      data: UserResolverService
    },
    children: [
      {
        path: '',
        redirectTo: 'detail',
        pathMatch: 'full'
      },
      {
        path: 'detail',
        component: DetailProductComponent
      },
      {
        path: 'comment',
        component: CommentProductComponent,
        // resolve: {
        //   item: ProductResolverService
        // }
      }
    ]
  },
  { path: 'add', component: PageAddProductComponent },
  { path: 'edit/:id', component: PageEditProductComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(
      appRoutes
    )
  ],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }
