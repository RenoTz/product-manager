import { Injectable } from '@angular/core';
import { Resolve, Router, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Product } from 'src/app/shared/models/product.model';
import { ProductService } from './product.service';
import { Observable, of, EMPTY } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductResolverService implements Resolve<Product>  {

  constructor(
    private ps: ProductService, private router: Router
  ) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Product> | Observable<never> {

    return  this.ps.product$.value ? of(this.ps.product$.value) : EMPTY;
  }
}
