import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Product } from '../../shared/models/product.model';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  private _collection$: Observable<Product[]>;
  private itemsCollection: AngularFirestoreCollection<Product>;

  product$: BehaviorSubject<Product> = new BehaviorSubject(null);

  constructor(
    private afs: AngularFirestore
  ) {
    this.itemsCollection = afs.collection<Product>('products');
    this._collection$ = this.itemsCollection.valueChanges().pipe(
      map((data) => {
        return data.map((obj) => {
          return new Product(obj);
        });
      })
    );
  }

  public get collection$(): Observable<Product[]> {
    return this._collection$;
  }

  public set collection$(col$: Observable<Product[]>) {
    this._collection$ = col$;
  }

  add(item: Product): Promise<any> {
    const id = this.afs.createId();
    const product = { id, ...item };
    return this.itemsCollection.doc(id).set(product).catch((e) => {
      console.log(e);
    });
  }

  update(item: Product): Promise<any> {
    const product = { ...item};
    return this.itemsCollection.doc(item.id).update(product).catch((e) => {
      console.log(e);
    });
  }

  delete(item: Product): Promise<any> {
    return this.itemsCollection.doc(item.id).delete().catch((e) => {
      console.log(e);
    });
  }

  getProduct(id: string): Observable<Product> {
    return this.itemsCollection.doc<Product>(id).valueChanges();
  }

  getProductsByType(typeProduct: string): Observable<Product[]> {
    return this.itemsCollection.valueChanges().pipe(
      map((data: Product[]) => {
        const tab = data.filter( (product) => product.typeProduct === typeProduct).map((product) => {
          return product;
        });
        this.product$.next(tab[0]);
        return tab;
      })
    );
  }
}
