import { Component, OnInit } from '@angular/core';
import { ProductService } from '../../services/product.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-comment-product',
  templateUrl: './comment-product.component.html',
  styleUrls: ['./comment-product.component.scss']
})
export class CommentProductComponent implements OnInit {

  constructor(
    public ps: ProductService,
    private router: Router
  ) {
  }

  ngOnInit() {
  }

  addComment(comment: string) {
    const product = this.ps.product$.value;
    product.comment = comment;
    this.ps.update(product);
    this.router.navigate(['/products/detail']);
  }

}
