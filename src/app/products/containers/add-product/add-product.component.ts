import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../../../shared/models/product.model';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

  constructor(
    private ps: ProductService,
    private router: Router
  ) { }

  ngOnInit() {
  }

  add(item: Product) {
    this.ps.add(item).then(() => {
      this.router.navigate(['/products']);
    });
  }

}
