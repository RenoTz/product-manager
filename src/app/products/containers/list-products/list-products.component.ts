import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Observable } from 'rxjs';
import { NavPage } from 'src/app/shared/models/nav-page.model';
import { Product } from '../../../shared/models/product.model';
import { ItemProductComponent } from '../../components/item-product/item-product.component';
import { ProductService } from '../../services/product.service';
import { MenuItem } from 'primeng/components/common/menuitem';
import { TypeProduct } from 'src/app/shared/enums/type-product.enum';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.scss']
})
export class ListProductsComponent implements OnInit {

  public collection$: Observable<Product[]>;

  @ViewChildren(ItemProductComponent) itemsProduct: QueryList<ItemProductComponent>;

  type = 'Aliment';
  labelButton = 'Ajouter un produit';
  title = 'Produits';
  navBar: MenuItem[] = [];
  items: MenuItem[] = [];

  public headers = [
    'DESIGNATION',
    'PRIX',
    'QUANTITE',
    'MAGASIN',
    'EDITER',
    'SUPPRIMER',
    'AJOUTER'
  ];


  constructor(private ps: ProductService) {
  }

  ngOnInit() {
    Object.values(TypeProduct).forEach(type => {
      this.navBar.push(new NavPage({ route: type, label: type}));
      this.items.push({ label: type});
    });
    if (this.type) {
      this.collection$ = this.ps.getProductsByType(this.type);
    }

  }

  update(item: Product) {
    this.ps.update(item).then((data) => {
      // traiter retour api
    });
  }

  removeClassActive() {
    this.itemsProduct.forEach(item => {
      item.removeClass();
    });
  }

  selectProduct(typeProduct: string) {
    if (this.type !== typeProduct) {
      this.type = typeProduct;
      this.collection$ = this.ps.getProductsByType(this.type);
    }
  }

}
