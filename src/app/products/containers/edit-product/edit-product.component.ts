import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Product } from 'src/app/shared/models/product.model';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {

  public product$: Observable<Product>;

  idPesta: string;

  constructor(
    private ps: ProductService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      this.idPesta = params.id;
      this.product$ = this.ps.getProduct(params.id);
    });
  }

  edit(item: Product) {
    item.id = this.idPesta;
    this.ps.update(item).then((data) => {
      this.router.navigate(['/products']);
    });
  }

}
