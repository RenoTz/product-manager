import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { Product } from '../../../shared/models/product.model';
import { ProductService } from '../../services/product.service';

@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.component.html',
  styleUrls: ['./detail-product.component.scss']
})
export class DetailProductComponent implements OnInit {

  public product$: Subject<Product>;

  constructor(
    private ps: ProductService
  ) { }

  ngOnInit() {
    this.product$ = this.ps.product$;
  }

}
