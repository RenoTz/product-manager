import { NavPage } from '../../../shared/models/nav-page.model';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-products',
  templateUrl: './page-products.component.html',
  styleUrls: ['./page-products.component.scss']
})
export class PageProductsComponent implements OnInit {

  title = 'Produits';


  navPage = [
    new NavPage({ route: 'detail', label: 'Detail'}),
    new NavPage({ route: 'comment', label: 'Commentaires'})
  ];

  constructor() { }

  ngOnInit() {
  }

}
