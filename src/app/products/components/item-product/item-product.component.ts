import { PanierService } from './../../../panier/services/panier.service';
import { TypeProduct } from './../../../shared/enums/type-product.enum';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { States } from '../../../shared/enums/states.enum';
import { Product } from '../../../shared/models/product.model';
import { ProductService } from '../../services/product.service';
import { runInThisContext } from 'vm';
import { Panier } from 'src/app/shared/models/panier.model';

@Component({
  selector: 'app-item-product',
  templateUrl: './item-product.component.html',
  styleUrls: ['./item-product.component.scss']
})
export class ItemProductComponent implements OnInit {
  @Input() item: Product;

  @Output() itemAndState: EventEmitter<Product> = new EventEmitter();
  @Output() clicked: EventEmitter<{}> = new EventEmitter();

  public states = States;
  public types = TypeProduct;
  public quantites = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

  colorTd = false;

  constructor(
    private ps: ProductService,
    private panierService: PanierService,
    private router: Router
  ) {}

  ngOnInit() {}

  update() {
    this.itemAndState.emit(this.item);
  }

  getDetail() {
    this.clicked.emit();
    this.ps.product$.next(this.item);
    this.colorTd = true;
  }

  removeClass() {
    this.colorTd = false;
  }

  redirectEditItem() {
    this.router.navigate(['products/edit', this.item.id]);
  }

  delete() {
    this.ps.delete(this.item);
  }

  addListeCourses() {
    this.panierService.panier$.subscribe((panier: Panier) => {
      const elt = panier.listeProducts.find(p => p.id === this.item.id);
      if (!elt) {
        this.panierService.addToPanier(this.item, panier);
      }
    });
  }
}
