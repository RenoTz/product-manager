import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Magasins } from 'src/app/shared/enums/magasins.enum';
import { TypeProduct } from 'src/app/shared/enums/type-product.enum';
import { States } from '../../../shared/enums/states.enum';
import { Product } from '../../../shared/models/product.model';

@Component({
  selector: 'app-form-product',
  templateUrl: './form-product.component.html',
  styleUrls: ['./form-product.component.scss']
})
export class FormProductComponent implements OnInit {

  states = States;
  types = TypeProduct;
  magasins = Magasins;

  @Output() nItem: EventEmitter<Product> = new EventEmitter();

  @Input() product: Product;

  init = new Product();
  form: FormGroup;

  constructor(private fb: FormBuilder) { }

  ngOnInit() {
    if (this.product) {
      this.init = this.product;
    }
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      typeProduct: [this.init.typeProduct],
      designation: [this.init.designation, Validators.compose([Validators.required, Validators.minLength(3)])],
      prix: [this.init.prix],
      quantite: [this.init.quantite],
      state: [this.init.state],
      magasin: [this.init.magasin],
      comment: [this.init.comment]
    });
  }

  onSubmit() {
    this.nItem.emit(this.form.value);
  }

}
