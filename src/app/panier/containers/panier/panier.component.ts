import { Component, Input, OnInit, QueryList, ViewChildren } from '@angular/core';
import { ItemProductComponent } from 'src/app/products/components/item-product/item-product.component';
import { Panier } from 'src/app/shared/models/panier.model';
import { Product } from 'src/app/shared/models/product.model';
import { StateClient } from '../../../shared/enums/state-client.enum';
import { PanierService } from '../../services/panier.service';

@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.scss']
})
export class PanierComponent implements OnInit {

  public states = StateClient;

  @Input() panier: Panier;

  listeProducts: Product[] = [];

  @ViewChildren(ItemProductComponent) items: QueryList<ItemProductComponent>;


  public headers = [
    'DESIGNATION',
    'PRIX',
    'QUANTITE',
    'MAGASIN'
  ];

  constructor(
    private ps: PanierService
  ) {
  }

  ngOnInit() {
    this.listeProducts = this.panier.listeProducts;
  }

  update() {
    this.ps.update(this.panier);
  }

  removeItem(item: Product) {
    const index = this.listeProducts.indexOf(item);
    this.listeProducts.splice(index, 1);
    this.ps.deleteItem(this.panier);
  }

  removeClassActive() {
    this.items.forEach(item => {
      item.removeClass();
    });
  }
}
