import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { States } from 'src/app/shared/enums/states.enum';
import { TypeProduct } from 'src/app/shared/enums/type-product.enum';
import { Product } from 'src/app/shared/models/product.model';

@Component({
  selector: 'app-item-panier',
  templateUrl: './item-panier.component.html',
  styleUrls: ['./item-panier.component.scss']
})
export class ItemPanierComponent implements OnInit {

  @Input() item: Product;

  @Output() itemAndState: EventEmitter<Product> = new EventEmitter();
  @Output() clicked: EventEmitter<{}> = new EventEmitter();
  @Output() remove: EventEmitter<Product> = new EventEmitter();

  public states = States;
  public types = TypeProduct;
  public quantites = [
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9
  ];

  colorTd = false;

  constructor() { }

  ngOnInit() {
  }

  update() {
    this.itemAndState.emit(this.item);
  }

  getDetail() {
    this.colorTd = true;
  }

  removeClass() {
    this.colorTd = false;
  }

  delete() {
    this.remove.emit(this.item);
  }



}
