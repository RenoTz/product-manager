import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve } from '@angular/router';
import { Panier } from 'src/app/shared/models/panier.model';
import { PanierService } from './panier.service';
import { EMPTY, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PanierResolverService implements Resolve<Panier> {

  constructor(
    private ps: PanierService
  ) { }

  resolve(route: ActivatedRouteSnapshot): Observable<Panier> | Observable<never> {
    if (this.ps.panier$) {
      return this.ps.panier$;
    } else {
      return EMPTY;
    }
  }
}
