import { TestBed } from '@angular/core/testing';

import { PanierResolverService } from './panier-resolver.service';

describe('PanierResolverService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PanierResolverService = TestBed.get(PanierResolverService);
    expect(service).toBeTruthy();
  });
});
