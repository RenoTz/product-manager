import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Client } from 'src/app/shared/models/client.model';

@Injectable({
  providedIn: 'root'
})
export class ClientService {
  private _collection$: Observable<Client[]>;
  private itemsCollection: AngularFirestoreCollection<Client>;

  constructor(private afs: AngularFirestore) {

    this.itemsCollection = afs.collection<Client>('clients');
    this._collection$ = this.itemsCollection.valueChanges().pipe(
      map(data => data.map(obj => new Client(obj)))
    );
  }

  public get collection$(): Observable<Client[]> {
    return this._collection$;
  }

  public set collection$(col$: Observable<Client[]>) {
    this._collection$ = col$;
  }

  // add client
  add(item: Client): Promise<any> {
    const id = this.afs.createId();
    const client = { id, ...item };
    return this.itemsCollection
      .doc(id)
      .set(client)
      .catch(e => {
        console.log(e);
      });
  }

  update(item: Client): Promise<any> {
    const client = { ...item };
    return this.itemsCollection
      .doc(item.id)
      .update(client)
      .catch(e => {
        console.log(e);
      });
  }

  public delete(item: Client): Promise<any> {
    return this.itemsCollection
      .doc(item.id)
      .delete()
      .catch(e => {
        console.log(e);
      });
  }

  getClient(id: string): Observable<Client> {
    return this.itemsCollection.doc<Client>(id).valueChanges();
  }
}
