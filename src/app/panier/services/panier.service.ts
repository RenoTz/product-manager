import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Observable, of } from 'rxjs';
import { Panier } from 'src/app/shared/models/panier.model';
import { Product } from 'src/app/shared/models/product.model';

@Injectable({
  providedIn: 'root'
})
export class PanierService {
  private _panier$: Observable<Panier>;
  private itemsCollection: AngularFirestoreCollection<Panier[]>;

  constructor(
    private afs: AngularFirestore
  ) {
    this.itemsCollection = afs.collection<Panier[]>('paniers');
  }

  public get panier$(): Observable<Panier> {
    return this._panier$;
  }

  public set panier$(panier$: Observable<Panier>) {
    this._panier$ = panier$;
  }

  create(): Observable<string> {
    const id = this.afs.createId();
    const panier: Panier = { id: id, listeProducts: [] };
    this.itemsCollection
      .doc(id)
      .set(panier)
      .catch(e => {
        console.log(e);
      });
    return of(id);
  }

  update(panier: Panier): Promise<any> {
    return this.itemsCollection
      .doc(panier.id)
      .update(panier)
      .catch(e => {
        console.log(e);
      });
  }

  addToPanier(item: Product, panier: Panier): Promise<any> {
    panier.listeProducts.push({ ...item });

    return this.itemsCollection
      .doc(panier.id)
      .update(panier)
      .catch(e => {
        console.log(e);
      });
  }

  deleteItem(panier: Panier): Promise<any> {
    return this.itemsCollection
      .doc(panier.id)
      .update(panier)
      .catch(e => {
        console.log(e);
      });
  }

  getPanier(id: string): Observable<Panier> {
    return this.itemsCollection.doc<Panier>(id).valueChanges();
  }
}
