import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { ItemPanierComponent } from './components/item-panier/item-panier.component';
import { PanierComponent } from './containers/panier/panier.component';
import { PagePanierComponent } from './pages/page-panier/page-panier.component';
import { PanierRoutingModule } from './panier-routing.module';

@NgModule({
  declarations: [
    PagePanierComponent,
    PanierComponent,
    ItemPanierComponent
  ],
  imports: [CommonModule, PanierRoutingModule, SharedModule]
})
export class PanierModule { }
