import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { Panier } from 'src/app/shared/models/panier.model';
import { PanierService } from './../../services/panier.service';

@Component({
  selector: 'app-page-panier',
  templateUrl: './page-panier.component.html',
  styleUrls: ['./page-panier.component.scss']
})
export class PagePanierComponent implements OnInit {
  title = 'Mon panier';

  panier$: Observable<Panier>;

  constructor(public ps: PanierService, private route: ActivatedRoute) {}

  ngOnInit() {
    if (this.ps.panier$) {
      this.panier$ = this.ps.panier$;
      // this.ps.panier$.subscribe((data) => {
      //   console.log('PagePanierComponent > panier$ => ', data);
      // });
    }
  }
}
