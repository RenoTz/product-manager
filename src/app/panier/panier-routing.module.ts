import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PagePanierComponent } from './pages/page-panier/page-panier.component';
import { UserResolverService } from '../user/services/user-resolver.service';

const appRoutes: Routes = [
  {
    path: '',
    component: PagePanierComponent,
    resolve: {
      data: UserResolverService
    }
 }
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(
      appRoutes
    )
  ],
  exports: [RouterModule]
})
export class PanierRoutingModule { }
