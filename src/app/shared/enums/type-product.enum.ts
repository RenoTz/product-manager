export enum TypeProduct {

  ALIMENT = 'Aliment',
  DESSERT = 'Dessert',
  FRUIT_LEGUME = 'Fruits&Légumes',
  BOISSON = 'Boisson',
  HYGIENE = 'Hygiène',
  COSMETIQUE = 'Cosmétique'
}
