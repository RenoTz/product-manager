export enum TypeHomeProduct {

  MEUBLE = 'Meuble',
  ELECTRO = 'Electroménager',
  DECORATION = 'Décoration',
  LIVRES = 'Livres',
  INFORMATIQUE = 'Informatique',
  JEUX_VIDEO = 'Jeux vidéo',
  TELEPHONIE = 'Téléphonie',
  LITERIE = 'Literie',
  AUTOMOBILE = 'Automobile',
  TV_SON_PHOTO = 'TV/Son/Photo'
}
