export enum Magasins {
    CARREFOUR = 'Carrefour',
    CASINO = 'Casino',
    INTERMARCHE = 'Intermarché',
    AUCHAN = 'Auchan',
    HEURE_DU_MARCHE = 'L\'Heure du marché'

}
