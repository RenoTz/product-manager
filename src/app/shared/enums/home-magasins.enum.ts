export enum HomeMagasins {
    CARREFOUR = 'Carrefour',
    CONFORAMA = 'Conforama',
    IKEA = 'Ikéa',
    BUT = 'BUT',
    CDISCOUNT = 'Cdiscount',
    LDLC = 'LDLC',
    AMAZON = 'Amazon',
    BOULANGER = 'Boulanger',
    JARDILAND = 'Jardiland'

}
