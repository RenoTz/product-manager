import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { FileUploadModule } from 'primeng/fileupload';
import { MenubarModule } from 'primeng/menubar';
import { ToastModule } from 'primeng/toast';
import { TemplatesModule } from '../template/templates.module';
import { BoutonComponent } from './components/bouton/bouton.component';
import { FormCommentComponent } from './components/form-comment/form-comment.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { NavPageComponent } from './components/nav-page/nav-page.component';
import { TableauComponent } from './components/tableau/tableau.component';
import { StateDirective } from './directives/state.directive';

@NgModule({
  declarations: [
    TableauComponent,
    StateDirective,
    BoutonComponent,
    NavPageComponent,
    FormCommentComponent,
    NavBarComponent,
  ],
  exports: [
    TableauComponent,
    StateDirective,
    TemplatesModule,
    BoutonComponent,
    FormCommentComponent,
    NavPageComponent,
    NavBarComponent,
    FormsModule,
    ReactiveFormsModule,
    FileUploadModule
  ],
  imports: [
    CommonModule,
    TemplatesModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    BreadcrumbModule,
    MenubarModule,
    FileUploadModule,
    ToastModule
  ]
})
export class SharedModule {}
