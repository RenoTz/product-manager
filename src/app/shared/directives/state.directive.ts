import { Directive, HostBinding, Input, OnChanges } from '@angular/core';

@Directive({
  selector: '[appState]'
})
export class StateDirective implements OnChanges {

  @Input() appState: number;
  @HostBinding('class') nomClass: string;

  constructor() {}

  ngOnChanges() {
    this.nomClass = this.formatClass(this.appState);
  }

  private formatClass(quantite: number): string {

    return quantite > 0 ? 'state-enstock' : 'state-epuise';
  }
}

// get un state
// en fonction du state, generate a class name
// bind this class name avec l'attribut class de l'element host
// (element sur lequel on applique cette directive)

// fn formatClass()
// En stock => state-enstock
// Epuisé => state-epuise
