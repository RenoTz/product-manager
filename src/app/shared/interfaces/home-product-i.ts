import { Url } from 'url';
import { HomeMagasins } from '../enums/home-magasins.enum';

export interface HomeProductI {
  id: string;
  typeProduct: string;
  prix: number;
  designation: string;
  magasin: HomeMagasins;
  nomFacture: string;
  comment: string;
  dateAchat: Date;
}
