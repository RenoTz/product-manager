export interface UserI {
  image: string;
  name: string;
  provider: string;
}
