import { Product } from './../models/product.model';

export interface PanierI {
  id: string;
  listeProducts: Product[];
}
