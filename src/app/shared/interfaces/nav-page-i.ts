
export interface NavPageI {
  route: string;
  label: string;
}
