export interface IColonne {
  field: string;
  header: string;
  sortableDisabled: boolean;
}
