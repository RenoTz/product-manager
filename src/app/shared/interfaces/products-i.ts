import { States } from '../enums/states.enum';
import { Magasins } from '../enums/magasins.enum';

export interface ProductsI {
  id: string;
  typeProduct: string;
  prix: number;
  quantite: number;
  designation: string;
  magasin: Magasins;
  state: States;
  comment: string;
}
