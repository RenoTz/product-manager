import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-comment',
  templateUrl: './form-comment.component.html',
  styleUrls: ['./form-comment.component.scss']
})
export class FormCommentComponent implements OnInit {

  @Input() comment: string;

  @Output() newCommment: EventEmitter<string> = new EventEmitter();

  form: FormGroup;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit() {
    this.createForm();
  }

  private createForm() {
    this.form = this.fb.group({
      comment: [this.comment, Validators.compose([Validators.required, Validators.maxLength(150)])]
    });
  }

  onSubmit() {
    const comment = this.form.get('comment').value;
    this.newCommment.emit(comment);
  }

}
