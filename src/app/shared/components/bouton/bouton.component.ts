import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-bouton',
  templateUrl: './bouton.component.html',
  styleUrls: ['./bouton.component.scss']
})
export class BoutonComponent implements OnInit {

  @Input() labelButton: string;

  constructor() { }

  ngOnInit() {
  }

}
