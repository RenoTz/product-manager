import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { MenuItem } from 'primeng/components/common/menuitem';
import { NavPage } from '../../models/nav-page.model';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent implements OnInit {
  @Input() listRoutes: NavPage[];
  @Input() title: string;
  @Input() typeProduct: MenuItem[];
  @Input() items: MenuItem[];
  @Output() select: EventEmitter<string> = new EventEmitter();

  labelButton = 'Ajouter';

  constructor(private router: Router) {}

  home: MenuItem;

  ngOnInit() {
    // this.home = {icon: 'pi pi-home'};
  }

  selectProduct(typeProduct: string) {
    this.select.emit(typeProduct);
  }
}
