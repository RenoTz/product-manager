import { Url } from 'url';
import { HomeMagasins } from '../enums/home-magasins.enum';
import { TypeHomeProduct } from '../enums/type-home-product.enum';
import { HomeProductI } from '../interfaces/home-product-i';

export class HomeProduct implements HomeProductI {
  id: string;
  typeProduct = TypeHomeProduct.MEUBLE;
  prix: number;
  designation: string;
  comment: string;
  magasin = HomeMagasins.CONFORAMA;
  nomFacture: string;
  dateAchat: Date;

  constructor(field?: Partial<HomeProduct>) {
    if ( field ) {
      Object.assign(this, field);
    }
  }

}
