import { StateClient } from '../enums/state-client.enum';
import { ClientI } from '../interfaces/client-i';

export class Client implements ClientI {
  id: string;
  name: string;
  email: string;
  state = StateClient.ACTIF;
  idPanier: string;

  constructor(field?: Partial<Client>) {
    if (field) {
      Object.assign(this, field);
    }
  }
}


