import { IColonne } from '../interfaces/colonne-i';

export class Colonne implements IColonne {
  field: string;
  header: string;
  sortableDisabled = false;

  constructor(fields?: Partial<Colonne>) {
    if (fields) {
      Object.assign(this, fields);
    }
  }
}
