import { UserI } from '../interfaces/user-i';

export class User implements UserI {
  image: string;
  name: string;
  provider: string;

  constructor() {
    this.image = '';
    this.name = '';
    this.provider = '';
  }
}
