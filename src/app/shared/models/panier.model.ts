import { PanierI } from './../interfaces/panier-i';
import { Product } from './product.model';

export class Panier implements PanierI {
  id: string;
  listeProducts: Product[] = [];

  constructor(field?: Partial<Panier>) {
    if(field) {
      Object.assign(this, field);
    }
  }
}
