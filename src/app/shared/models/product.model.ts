import { States } from '../enums/states.enum';
import { TypeProduct } from '../enums/type-product.enum';
import { ProductsI } from '../interfaces/products-i';
import { Magasins } from '../enums/magasins.enum';

export class Product implements ProductsI {
  id: string;
  typeProduct = TypeProduct.ALIMENT;
  prix: number;
  quantite = 1;
  designation: string;
  state =  States.EN_STOCK;
  comment: string;
  magasin = Magasins.INTERMARCHE;

  constructor(field?: Partial<Product>) {
    if ( field ) {
      Object.assign(this, field);
    }
  }

}
