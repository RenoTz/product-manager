import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListHomeProductsComponent } from './list-home-products.component';

describe('ListHomeProductsComponent', () => {
  let component: ListHomeProductsComponent;
  let fixture: ComponentFixture<ListHomeProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListHomeProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListHomeProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
