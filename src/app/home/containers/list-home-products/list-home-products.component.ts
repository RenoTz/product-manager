import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Observable } from 'rxjs';
import { TypeHomeProduct } from 'src/app/shared/enums/type-home-product.enum';
import { HomeProduct } from 'src/app/shared/models/home-product.model';
import { NavPage } from 'src/app/shared/models/nav-page.model';
import { ItemHomeProductComponent } from '../../components/item-home-product/item-home-product.component';
import { HomeProductService } from '../../services/home-product.service';
import { MenuItem } from 'primeng/components/common/menuitem';

@Component({
  selector: 'app-list-home-products',
  templateUrl: './list-home-products.component.html',
  styleUrls: ['./list-home-products.component.scss']
})
export class ListHomeProductsComponent implements OnInit {

  public collection$: Observable<HomeProduct[]>;

  @ViewChildren(ItemHomeProductComponent) itemsHomeProduct: QueryList<ItemHomeProductComponent>;

  type = 'Meuble';
  labelButton = 'Ajouter un produit';
  title = 'Home';
  navBar: MenuItem[] = [];
  items: MenuItem[] = [];

  public headers = [
    'DESIGNATION',
    'PRIX',
    'MAGASIN',
    'FACTURE',
    'EDITER',
    'SUPPRIMER'
  ];

  constructor(private hps: HomeProductService) { }

  ngOnInit() {
    Object.values(TypeHomeProduct).forEach(type => {
      this.navBar.push(new NavPage({ route: type, label: type}));
      this.items.push({ label: type });
    });
    if (this.type) {
      this.collection$ = this.hps.getHomeProductsByType(this.type);
    }
  }

  update(item: HomeProduct) {
    this.hps.update(item).then((data) => {
      // traiter retour api
    });
  }

  removeClassActive() {
    this.itemsHomeProduct.forEach(item => {
      item.removeClass();
    });
  }

  selectHomeProduct(typeHomeProduct: string) {
    if (this.type !== typeHomeProduct) {
      this.type = typeHomeProduct;
      this.collection$ = this.hps.getHomeProductsByType(this.type);
    }
  }

}
