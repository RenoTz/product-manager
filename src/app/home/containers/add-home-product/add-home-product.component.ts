import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HomeProduct } from 'src/app/shared/models/home-product.model';
import { HomeProductService } from '../../services/home-product.service';
import { FileService } from './../../services/file.service';

@Component({
  selector: 'app-add-home-product',
  templateUrl: './add-home-product.component.html',
  styleUrls: ['./add-home-product.component.scss']
})
export class AddHomeProductComponent implements OnInit {

  constructor(
    private hps: HomeProductService,
    private fileService: FileService,
    private router: Router
  ) { }

  ngOnInit() {}

  add($event) {
    const item = $event.item;
    const file = $event.file;
    if (file) {
      this.fileService.uploadFile(file).then(() => {
        item.nomFacture = file.name;
        this.addAndRedirect(item);
      });
    } else {
      this.addAndRedirect(item);
    }
  }

  private addAndRedirect(item: HomeProduct) {
    this.hps.add(item).then(() => {
      this.router.navigate(['/home-products']);
    });
  }

}
