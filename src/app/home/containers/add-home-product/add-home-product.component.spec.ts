import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AddHomeProductComponent } from './add-home-product.component';


describe('AddHomeProductComponent', () => {
  let component: AddHomeProductComponent;
  let fixture: ComponentFixture<AddHomeProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddHomeProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddHomeProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
