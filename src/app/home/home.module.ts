import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { FormHomeProductComponent } from './components/form-home-product/form-home-product.component';
import { ItemHomeProductComponent } from './components/item-home-product/item-home-product.component';
import { AddHomeProductComponent } from './containers/add-home-product/add-home-product.component';
import { ListHomeProductsComponent } from './containers/list-home-products/list-home-products.component';
import { HomeRoutingModule } from './home-routing.module';
import { PageAddHomeProductComponent } from './pages/page-add-home-product/page-add-home-product.component';
import { PageHomeProductsComponent } from './pages/page-home-products/page-home-products.component';

@NgModule({
  declarations: [
    PageHomeProductsComponent,
    PageAddHomeProductComponent,
    ListHomeProductsComponent,
    ItemHomeProductComponent,
    AddHomeProductComponent,
    FormHomeProductComponent,
  ],
  imports: [CommonModule, SharedModule, HomeRoutingModule]
})
export class HomeModule {}
