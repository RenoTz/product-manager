import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PageAddHomeProductComponent } from './page-add-home-product.component';


describe('PageAddHomeProductComponent', () => {
  let component: PageAddHomeProductComponent;
  let fixture: ComponentFixture<PageAddHomeProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageAddHomeProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageAddHomeProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
