import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageHomeProductsComponent } from './page-home-products.component';

describe('PageHomeProductsComponent', () => {
  let component: PageHomeProductsComponent;
  let fixture: ComponentFixture<PageHomeProductsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageHomeProductsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageHomeProductsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
