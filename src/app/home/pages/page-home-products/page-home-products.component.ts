import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-home-products',
  templateUrl: './page-home-products.component.html',
  styleUrls: ['./page-home-products.component.scss']
})
export class PageHomeProductsComponent implements OnInit {

  title = 'Maison';

  constructor() { }

  ngOnInit() {
  }

}
