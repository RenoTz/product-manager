import { Injectable } from '@angular/core';
import * as firebase from 'firebase';

@Injectable({
  providedIn: 'root'
})
export class FileService {

  private pathFacture = 'factures/';
  private storageRef: firebase.storage.Reference;

  constructor() {
    this.storageRef = firebase.storage().ref();
  }

  uploadFile(file: File): Promise<any> {

    const metadata = {
      contentType: 'application/pdf'
    };

    return this.storageRef.child(this.pathFacture + file.name).put(file, metadata).then((snapshot) => {
      return snapshot;
    });
  }

  downloadFile(nameFile: string): Promise<any> {
    return this.storageRef.child(this.pathFacture + nameFile).getDownloadURL();
  }

  delete(nameFile: string): Promise<any> {
    return this.storageRef.child(this.pathFacture + nameFile).delete();
  }

}
