import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import * as firebase from 'firebase';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HomeProduct } from 'src/app/shared/models/home-product.model';
import { Url } from 'url';

@Injectable({
  providedIn: 'root'
})
export class HomeProductService {
  private _collection$: Observable<HomeProduct[]>;
  private itemsCollection: AngularFirestoreCollection<HomeProduct>;

  product$: BehaviorSubject<HomeProduct> = new BehaviorSubject(null);

  url: Url;

  constructor(private afs: AngularFirestore) {
    this.itemsCollection = afs.collection<HomeProduct>('home_products');
    this._collection$ = this.itemsCollection.valueChanges().pipe(
      map(data => {
        return data.map(obj => {
          return new HomeProduct(obj);
        });
      })
    );
  }

  public get collection$(): Observable<HomeProduct[]> {
    return this._collection$;
  }

  public set collection$(col$: Observable<HomeProduct[]>) {
    this._collection$ = col$;
  }

  add(item: HomeProduct): Promise<any> {
    const id = this.afs.createId();
    const product = { id, ...item };

    return this.itemsCollection
    .doc(product.id)
    .set(product)
    .catch(e => {
      console.log(e);
    });
  }

  update(item: HomeProduct): Promise<any> {
    const product = { ...item };
    return this.itemsCollection
      .doc(item.id)
      .update(product)
      .catch(e => {
        console.log(e);
      });
  }

  delete(item: HomeProduct): Promise<any> {
    return this.itemsCollection
      .doc(item.id)
      .delete()
      .catch(e => {
        console.log(e);
      });
  }

  getProduct(id: string): Observable<HomeProduct> {
    return this.itemsCollection.doc<HomeProduct>(id).valueChanges();
  }

  getHomeProductsByType(typeProduct: string): Observable<HomeProduct[]> {
    return this.itemsCollection.valueChanges().pipe(
      map((data: HomeProduct[]) => {
        const tab = data
          .filter(product => product.typeProduct === typeProduct)
          .map(product => {
            return product;
          });
        this.product$.next(tab[0]);
        return tab;
      })
    );
  }

  // uploadFile(file: File, item: HomeProduct): Promise<any> {

  //   const promise = new Promise((resolve, reject) => {
  //     // Create a root reference
  //     const storageRef = firebase.storage().ref();

  //     const metadata = {
  //       contentType: 'application/pdf'
  //     };

  //     const uploadTask = storageRef.child('factures/' + file.name).put(file, metadata);

  //     // Listen for state changes, errors, and completion of the upload.
  //     uploadTask.on(
  //       firebase.storage.TaskEvent.STATE_CHANGED, // or 'state_changed'
  //       function(snapshot: Storage) {
  //         // Get task progress, including the number of bytes uploaded and the total number of bytes to be uploaded
  //         const progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
  //         console.log('Upload is ' + progress + '% done');
  //         switch (snapshot.state) {
  //           case firebase.storage.TaskState.PAUSED: // or 'paused'
  //             console.log('Upload is paused');
  //             break;
  //           case firebase.storage.TaskState.RUNNING: // or 'running'
  //             console.log('Upload is running');
  //             break;
  //         }
  //       },
  //       function(error) {
  //         console.log('error => ', error.message);
  //       },
  //       function() {
  //         // Upload completed successfully, now we can get the download URL
  //         uploadTask.snapshot.ref.getDownloadURL().then(function(downloadURL) {
  //           console.log('File available at', downloadURL);
  //           return downloadURL;
  //         });
  //         resolve();
  //       }
  //     );
  //   });
  //   return promise;
  // }

}
