import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserResolverService } from '../user/services/user-resolver.service';
import { PageAddHomeProductComponent } from './pages/page-add-home-product/page-add-home-product.component';
import { PageHomeProductsComponent } from './pages/page-home-products/page-home-products.component';

const appRoutes: Routes = [
  {
    path: '',
    component: PageHomeProductsComponent,
    resolve: {
      data: UserResolverService
    }
 },
 { path: 'add', component: PageAddHomeProductComponent },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(
      appRoutes
    )
  ],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
