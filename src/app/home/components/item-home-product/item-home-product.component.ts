import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { TypeHomeProduct } from 'src/app/shared/enums/type-home-product.enum';
import { HomeProduct } from 'src/app/shared/models/home-product.model';
import { HomeProductService } from '../../services/home-product.service';
import { FileService } from './../../services/file.service';

@Component({
  selector: 'app-item-home-product',
  templateUrl: './item-home-product.component.html',
  styleUrls: ['./item-home-product.component.scss']
})
export class ItemHomeProductComponent implements OnInit {

  @Input() item: HomeProduct;

  @Output() itemAndState: EventEmitter<HomeProduct> = new EventEmitter();
  @Output() clicked: EventEmitter<{}> = new EventEmitter();

  public types = TypeHomeProduct;

  colorTd = false;

  constructor(
    private hps: HomeProductService,
    private fileService: FileService,
    private router: Router
  ) {}

  ngOnInit() {}

  update() {
    this.itemAndState.emit(this.item);
  }

  getDetail() {
    this.clicked.emit();
    this.hps.product$.next(this.item);
    this.colorTd = true;
  }

  removeClass() {
    this.colorTd = false;
  }

  redirectEditItem() {
    this.router.navigate(['home-products/edit', this.item.id]);
  }

  delete() {
    if (this.item.nomFacture) {
      this.fileService.delete(this.item.nomFacture).then(() => {
        this.hps.delete(this.item);
      });
    } else {
      this.hps.delete(this.item);
    }
  }

  openFacture() {
    if (this.item.nomFacture) {
      this.fileService.downloadFile(this.item.nomFacture).then(url => {
        window.open(url, '_blank');
      });
    }
  }

}
