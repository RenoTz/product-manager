import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemHomeProductComponent } from './item-home-product.component';

describe('ItemHomeProductComponent', () => {
  let component: ItemHomeProductComponent;
  let fixture: ComponentFixture<ItemHomeProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemHomeProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemHomeProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
