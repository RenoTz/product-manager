import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { HomeMagasins } from 'src/app/shared/enums/home-magasins.enum';
import { TypeHomeProduct } from 'src/app/shared/enums/type-home-product.enum';
import { HomeProduct } from 'src/app/shared/models/home-product.model';

@Component({
  selector: 'app-form-home-product',
  templateUrl: './form-home-product.component.html',
  styleUrls: ['./form-home-product.component.scss']
})
export class FormHomeProductComponent implements OnInit {
  @Input() product: HomeProduct;
  @Output() nItem: EventEmitter<{item: HomeProduct; file: File}> = new EventEmitter();
  types = TypeHomeProduct;
  magasins = HomeMagasins;
  init = new HomeProduct();
  form: FormGroup;
  fileUpload: File;

  constructor(
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    if (this.product) {
      this.init = this.product;
    }
    this.createForm();
  }

  createForm() {
    this.form = this.fb.group({
      typeProduct: [this.init.typeProduct],
      designation: [this.init.designation, Validators.compose([Validators.required, Validators.minLength(3)])],
      prix: [this.init.prix],
      magasin: [this.init.magasin],
      comment: [this.init.comment]
    });
  }

  onSubmit() {
    const item = this.form.value;
    const file = this.fileUpload;
    this.nItem.emit({ item, file });
  }

  onFileChange(event) {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);
      this.fileUpload = file;
    }
  }
}
