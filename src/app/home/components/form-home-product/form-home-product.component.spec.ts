import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormHomeProductComponent } from './form-home-product.component';


describe('FormHomeProductComponent', () => {
  let component: FormHomeProductComponent;
  let fixture: ComponentFixture<FormHomeProductComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormHomeProductComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormHomeProductComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
