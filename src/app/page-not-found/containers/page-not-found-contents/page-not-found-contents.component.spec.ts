import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNotFoundContentsComponent } from './page-not-found-contents.component';

describe('PageNotFoundContentsComponent', () => {
  let component: PageNotFoundContentsComponent;
  let fixture: ComponentFixture<PageNotFoundContentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageNotFoundContentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNotFoundContentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
