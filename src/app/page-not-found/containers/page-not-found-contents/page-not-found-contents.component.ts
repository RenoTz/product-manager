import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-page-not-found-contents',
  templateUrl: './page-not-found-contents.component.html',
  styleUrls: ['./page-not-found-contents.component.scss']
})
export class PageNotFoundContentsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
