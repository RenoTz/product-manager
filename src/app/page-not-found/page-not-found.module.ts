import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { PageNotFoundContentsComponent } from './containers/page-not-found-contents/page-not-found-contents.component';
import { PageNotFoundRoutingModule } from './page-not-found-routing.module';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';

@NgModule({
  declarations: [PageNotFoundComponent, PageNotFoundContentsComponent],
  imports: [
    CommonModule,
    PageNotFoundRoutingModule
  ]
})
export class PageNotFoundModule { }
